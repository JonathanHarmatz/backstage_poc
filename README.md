# [Backstage](https://backstage.io)

This is your newly scaffolded Backstage App, Good Luck!
To start the app locally, run:

```sh
yarn install
yarn dev
```

<h3>For deployments 

## Environment variables:

**For Backstage build:**

    - BASE_URL - Address on which the server is deployed
    - GITLAB_HOST- Address of the gitlab instance (currently gitlab.com)
    - GITLAB_GROUP - Sets the Gitlab Discovery to crawl a specific group by name (name+ID format)
    - GITLAB_TOKEN - Gitlab token with access priviliges to the relvent projects
    - AZURE_CLIENT_ID - ID taken from the registered application in EntraID
    - AZURE_CLIENT_SECRED - Secret taken from the registered application in EntraID
    - AZURE_TENANT_ID - ID taken from the registered application in EntraID

**For backstage deployment (using AWS Lightsail):**

    - AWS_ACCESS_KEY_ID
    - AWS_ACCOUNT_ID
    - AWS_SECRET_ACCESS_KEY

## Authentication
Using Azure's entra id - requires a user's component for each user trying to authneticated.
User components can be added using a gitlab repo with the relevent data, as in this [example](https://gitlab.com/backstage8581127/backstage_users)

## Templates
Are discovered using the Gitlab discovery featue or manually added using the UI or through app-config.yaml. Example template is _AWS VPC Deployment_, which is manually added on serverside and triggers a CICD which runs a terraform file to deploy a VPC. The template itself can be found [here](https://gitlab.com/JonathanHarmatz/backstage_templates/-/blob/main/aws-ec2-deploy.yaml?ref_type=heads).

## CICD
Created an example of a simple deployment flow - which runs yarn build, builds a docker image and deploys the image to Lightsail. Can be switched to any image deployment tool chosen.

